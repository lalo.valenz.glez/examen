import angular from "angular";
import angularMeteor from "angular-meteor";

import { Profesores } from '../imports/collections/profesores';
import { Materias } from '../imports/collections/materias';
import { Estudiantes } from '../imports/collections/estudiantes';

angular.module('examen', [angularMeteor])
.controller('PrincipalController', function($scope){
    $scope.botones = {
        estudiantes: true,
        materias: false,
        profesores: false,
    }

    $scope.cambiar = function(num){
        switch (num) {
            case 1:
                $scope.botones.estudiantes = true;
                $scope.botones.materias = false;
                $scope.botones.profesores = false;
                break;
            case 2:
                $scope.botones.estudiantes = false;
                $scope.botones.materias = true;
                $scope.botones.profesores = false;
                break;
            case 3:
                $scope.botones.estudiantes = false;
                $scope.botones.materias = false;
                $scope.botones.profesores = true;
                break;
        }
    }
})
.controller('EstudiantesController', function($scope){
    setTimeout(function(){
        $scope.est = {};
        $scope.estudiantes = Estudiantes.find({}).fetch();
        $scope.$apply();
        $scope.agregarNuevo = function(nuevo){
            $scope.estudiantes.push(nuevo);
            Estudiantes.insert(nuevo);
            est = {};
        }

        $scope.eliminar = function(estudiante){
            Estudiantes.remove(estudiante._id);
        }

    }, 1000);
    

})
.controller('MateriasController', function($scope){
    setTimeout(function(){
        $scope.mat = {};
        $scope.materias = Materias.find({}).fetch();
        $scope.$apply();
        $scope.agregar = function(nueva){
            $scope.materias.push(nueva);
            Materias.insert(nueva);
            mat = {};
        }

        $scope.eliminar = function(materia){
            Materias.remove(materia._id);
        }

    }, 1000);
})
.controller('ProfesoresController', function($scope){
    setTimeout(function(){
        $scope.prof = {};
        $scope.profesores = Profesores.find({}).fetch();
        $scope.$apply();
        $scope.agregar = function(nuevo){
            $scope.profesores.push(nuevo);
            Profesores.insert(nuevo);
            prof = {};
        }
        $scope.eliminar = function(profesor){
            Profesores.remove(profesor._id);
        }

    }, 1000);
})