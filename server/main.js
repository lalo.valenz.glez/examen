import { Meteor } from 'meteor/meteor';
import { Profesores } from '../imports/collections/profesores';
import { Materias } from '../imports/collections/materias';
import { Estudiantes } from '../imports/collections/estudiantes';

Meteor.startup(() => {
  console.log(Estudiantes.find({}).fetch());
});
